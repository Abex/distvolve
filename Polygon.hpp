#ifndef POLYGON_HPP_INCLUDED
#define POLYGON_HPP_INCLUDED
#include <vector>
#include <functional>
#include <fstream>

typedef unsigned short dimension;
typedef unsigned char byte;

namespace distvolve {
class Image {
public:
  void set(std::vector<byte> data, dimension width, dimension height); // sets the images values

  std::vector<byte> data;
  dimension width;
  dimension height;
};

class Polygon {
public:
  Polygon(dimension width,dimension height,std::ifstream &stream); // load from ifstream (dna)
  Polygon(dimension width,dimension height,float load,std::function <float()> rand);// create a random polygon

  void drawInternal(); // Call this before any draw* fns

  void drawOn(Image &on,Image &from);
  void drawOnFromDna(Image &on);
  bool drawOnIfBetter(Image &on,Image &from);

  unsigned int fitness(byte* one,byte* two); // returns fitness between pixel 1 & 2
  dimension random(dimension min,dimension max); // unbaised random
  std::vector<byte> serialize();

  std::vector<dimension> points;
  std::vector<bool> internalMask; // actually bitset-like
  byte colour[4]; //RGBA

  dimension width;
  dimension height;

  dimension centerx; //center of the poly
  dimension centery;
};
}
#endif // POLYGON_HPP_INCLUDED
