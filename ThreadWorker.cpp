#include <Polygon.hpp>
#include <mutex>
#include <atomic>
#include <iostream>
#include <vector>
#include <map>
#include <random>

using namespace std;
using namespace distvolve;

extern Image inputImage;
extern Image outputImage;
extern vector<byte> polys; // serialized polygons
extern mutex mPoly; // lock for this array
extern atomic<int> generations;
extern atomic<int> polygons;
extern atomic<bool> run; // run the worker threads

void doWork(char* identifier) // thread fn
{
  float currentPolySize=.8; // Polygon size hinting

  int numFails=0; // how many fails in a row
  srand(time(NULL));// seed this threads RNG
  default_random_engine generator; // create standard deviation RNG
  normal_distribution<float> distribution(0.0,0.7);
  while(run) // main loop
  {
    distvolve::Polygon poly(inputImage.width,inputImage.height, // create a poly with max size&width, hinted by currentPolySize
                            currentPolySize,[&distribution,&generator]()->float {return distribution(generator);});//and using this threads premade RNG in a lambda
    poly.drawInternal(); // draw the polygon on its internal bitarray
    if(poly.drawOnIfBetter(outputImage,inputImage)) // returns true if this polygon is better. Also draws on if better.
    { // better
      mPoly.lock(); //lock poly
      vector<byte> vNewPoly = poly.serialize();
      polys.insert(polys.end(),vNewPoly.begin(),vNewPoly.end()); // append new bytes
      mPoly.unlock(); //unlock poly
      polygons++; // increase poly count stat
      numFails=0; // reset numFails (for size hinting)
    }
    else
    { // worse
      if(currentPolySize>.03 && numFails++>400) // Alot of failed generations
      {
        currentPolySize*=.08; // reduce size hint by 8%
        numFails=0;
      }
    }
    generations++; // increase gen count stat
  }
}
